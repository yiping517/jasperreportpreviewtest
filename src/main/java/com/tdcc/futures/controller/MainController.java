package com.tdcc.futures.controller;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tdcc.futures.dao.CityDao;
import com.tdcc.futures.entities.City;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.base.JRBaseTextField;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


/**
 * 
 * @author sarah
 * @creation 2021年2月3日上午9:59:01
 */
@Controller
public class MainController {
	
	private static Logger log = LoggerFactory.getLogger(MainController.class);
	
	@Autowired
	CityDao dao;
	
	/**
	 * 網頁根目錄
	 * @return login.html
	 */
	@RequestMapping(path="/")
	public String page_root() {
		log.info("MainController : page_root");
		
		try {
			String filePath = "C:\\kjsoft\\workspace\\jasperReportTest\\src\\main\\resources\\category1\\jrxml\\CityReport.jrxml";
			//String generatedFilePath = filePath + File.separator + "FirstReport.pdf";
			//跟jrxml同一個目錄會出現error
			
			String generatedFilePath = "C:\\kjsoft\\workspace\\jasperReportTest\\src\\main\\resources\\category1\\jrxml\\PDF" + File.separator + "CityReport.pdf";
			
			//set the value for the parameters in jasper report
			List<City> list = new ArrayList<City>();
			
			List<Map<String, Object>> result = dao.getQueryList();
			for(Map<String, Object> map : result) {
				City city = new City();
				city.setId((Integer)map.get("id"));
				city.setName((String)map.get("name"));
				city.setPopulation((Integer)map.get("population"));
				list.add(city);
			}
			
			//pass data source to jasper report
			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
			
			JasperReport report = JasperCompileManager.compileReport(filePath);
			
			//runtime change
//			JRBaseTextField textField = (JRBaseTextField)report.getTitle().getElementByKey("name");
//			textField.setForecolor(Color.orange);
			
			JasperPrint print = JasperFillManager.fillReport(report, null, dataSource);
			JasperExportManager.exportReportToPdfFile(print, generatedFilePath);
			System.out.println("Report created...");
			
		}catch(Exception e) {
			System.out.println("exception while creating report");
			e.printStackTrace();
		}
		
		
		return "index.html";
	}
	
	@GetMapping(value = "/previewPDFile")
	public void showPdf(HttpServletResponse response) throws IOException {
	    response.setContentType("application/pdf");
	    String filename = "C:\\kjsoft\\workspace\\jasperReportTest\\src\\main\\resources\\category1\\jrxml\\PDF" + File.separator + "CityReport.pdf";
	    InputStream inputStream = new FileInputStream(new File(filename));
	    int nRead;
	    while ((nRead = inputStream.read()) != -1) {
	        response.getWriter().write(nRead);
	    }
	} 
}
