/**
 * 版本：1.0
 * 	(1)	2018-05-16 11:00	Jimmy, Zekly
 * 		初版。
 */
package com.tdcc.futures.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.sql.Types;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.springframework.stereotype.Component;


/**
 * 工具類：(元件類)資料庫函數工具。
 * @author Jimmy, Zekly
 * @category 工具類。
 */
@Component
public class DBUtil {
	
	private static Logger log = LoggerFactory.getLogger(DBUtil.class);
	// 資料庫欄位命名方式。
	private enum JdbcAffixType {
		WITH_ORIGINAL_COLUMNS, 
		WITH_NAMED_COLUMNS, 
		WITH_NO_NAME_COLUMNS, 
		WITH_NUMBER_COLUMNS
	}
	
	@Autowired
	private JdbcTemplate jdbc;
	
	/**
	 * 取得連線
	 * @return
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		return jdbc.getDataSource().getConnection();
	}
	
	/**
	 * 配置欄位名稱。
	 * @param rs 資料列表。
	 * @return 本類內容的參數。
	 * @throws SQLException
	 */
//	public List<String> getOriginColumns(ResultSet rs) throws SQLException {
//		return this.getColumns(JdbcAffixType.WITH_ORIGINAL_COLUMNS, null, rs);
//	}

	/**
	 * 配置欄位名稱。
	 * @param rs 資料列表。
	 * @param namedColumns 指定欄位名稱。
	 * @return 本類內容的參數。
	 * @throws SQLException
	 */
//	public List<String> getNamedColumns(ResultSet rs, String[] namedColumns) throws SQLException {
//		return this.getColumns(JdbcAffixType.WITH_NAMED_COLUMNS, namedColumns, rs);
//	}
	
	/**
	 * 配置欄位名稱。
	 * @param rs 資料列表。
	 * @param noNameColumns 欄位名稱的陣列。
	 * @return 本類內容的參數。
	 * @throws SQLException
	 */
//	public List<String> getNoNamedColumns(ResultSet rs, String[] noNameColumns) throws SQLException {
//		return this.getColumns(JdbcAffixType.WITH_NO_NAME_COLUMNS, noNameColumns, rs);
//	}

	/**
	 * 配置欄位名稱。
	 * @param rs 資料列表。
	 * @param prefixName 數字名稱的前置字元。
	 * @return 本類內容的參數。
	 * @throws SQLException
	 */
//	public List<String> getNumberColumns(ResultSet rs, String prifixName) throws SQLException {
//		return this.getColumns(JdbcAffixType.WITH_NUMBER_COLUMNS, prifixName, rs);
//	}

	/**
	 * 使用原始欄位名稱查詢Store Procdure。
	 * @param sql SQL語法。
	 * @param args SQL語法的參數。
	 * @return 查詢結果。
	 * @throws CustomException 
	 */
//	public List<Map<String, Object>> queryWithOriginalColumns(String sql, Object... args) throws CustomException {
//		return this.query(JdbcAffixType.WITH_ORIGINAL_COLUMNS, null, sql, args);
//	}
	
	/**
	 * 使用指定欄位名稱查詢Store Procdure。
	 * @param namedColumns 指定欄位名稱。
	 * @param sql SQL語法。
	 * @param args SQL語法的參數。
	 * @return 查詢結果。
	 * @throws CustomException 
	 */
//	public List<Map<String, Object>> queryWithNamedColumns(String[] namedColumns, String sql, Object... args) throws CustomException {
//		return this.query(JdbcAffixType.WITH_NAMED_COLUMNS, namedColumns, sql, args);
//	}

	/**
	 * 當發現欄位名稱是 "空值" 時，使用指定參數當欄位名稱查詢Store Procdure。
	 * @param noNameColumns 欄位名稱的陣列。
	 * @param sql SQL語法。
	 * @param args SQL語法的參數。
	 * @return 查詢結果。
	 * @throws CustomException 
	 */
//	public List<Map<String, Object>> queryWithNoNameColumns(String[] noNameColumns, String sql, Object... args) throws CustomException {
//		return this.query(JdbcAffixType.WITH_NO_NAME_COLUMNS, noNameColumns, sql, args);
//	}

	/**
	 * 4. 使用 "數字" 當資料欄位名稱查詢Store Procdure。
	 * 調用 JDBC 方法，並使用數字名稱(如：F0, F1)當作返回的欄位民菜。
	 * @param prefixName 數字名稱的前置字元。如："F"，預設值為"F"。
	 * @param sql SQL語法。
	 * @param args SQL語法的參數。
	 * @return 查詢結果。
	 * @throws CustomException 
	 */
//	public List<Map<String, Object>> queryWithNumberColumns(String prefixName, String sql, Object... args) throws CustomException {
//		return this.query(JdbcAffixType.WITH_NUMBER_COLUMNS, prefixName, sql, args);
//	}

	public List<Map<String, Object>> queryForList(String sql) {

		List<Map<String, Object>> result = jdbc.queryForList(sql);

		for (Map<String, Object> row : result) {
			Iterator<Map.Entry<String, Object>> iterator = row.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, Object> mapEntry = (Map.Entry<String, Object>) iterator.next();
//				if (mapEntry.getValue() instanceof String) {
//					mapEntry.setValue(StringUtil.iso1ToBig5(mapEntry.getValue().toString()));
//				}
			}
		}
		return result;
	}
	
	public List<Map<String, Object>> queryForList(String sql, Object... params) {

		List<Map<String, Object>> result = jdbc.queryForList(sql, params);

		for (Map<String, Object> row : result) {
			Iterator<Map.Entry<String, Object>> iterator = row.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, Object> mapEntry = (Map.Entry<String, Object>) iterator.next();
//				if (mapEntry.getValue() instanceof String) {
//					mapEntry.setValue(StringUtil.iso1ToBig5((String) mapEntry.getValue()));
//				}
			}
		}
		return result;
	}
	
//	public SqlRowSet queryForRowSet(String sql) {
//		SqlRowSet rowSet = jdbc.queryForRowSet(StringUtil.big5ToIso1(sql));
//		return rowSet;
//	}
	
//	public SqlRowSet queryForRowSet(String sql, Object... params) {
//		for (int i = 0; i < params.length; i++) {
//			if (params[i] instanceof String) {
//				params[i] = StringUtil.big5ToIso1(params[i].toString());
//			}
//		}
//		SqlRowSet rowSet = jdbc.queryForRowSet(StringUtil.big5ToIso1(sql), params);
//		return rowSet;
//	}
	
//	public Map<String, Object> queryForMap(String sql) {
//		return jdbc.queryForMap(StringUtil.big5ToIso1(sql));
//	}
	
	public Map<String, Object> queryForMap(String sql, Object... params) {

		Map<String, Object> result = this.jdbc.queryForMap(sql, params);
		
		Iterator<Entry<String, Object>> iterator = result.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, Object> mapEntry = (Entry<String, Object>) iterator.next();
//			if (mapEntry.getValue() instanceof String) {
//				mapEntry.setValue(StringUtil.iso1ToBig5((String) mapEntry.getValue()));
//			}
		}
		return result;
	}

	@SuppressWarnings("hiding")
	public <T> T queryForObject(String sql, Class<T> requireType) {
		try {
			return this.jdbc.queryForObject(sql, requireType);
		} catch (Exception ex) {
			return null;
		}
	}
	
	@SuppressWarnings("hiding")
	public <T> T queryForObject(String sql, Object[] args, Class<T> requiredType) {
		try {
			return this.jdbc.queryForObject(sql, args, requiredType);
		} catch (Exception ex) {
			return null;
		}
	}
	
	@SuppressWarnings("hiding")
	public <T> T queryForObject(String sql, Object[] args, RowMapper<T> rowMapper) {
		try {
			return this.jdbc.queryForObject(sql, args, rowMapper);
		} catch (Exception ex) {
			return null;
		}
	}
	
//	public Integer update(String sql, Object... params) {
//		for (int i = 0; i < params.length; i++) {
//			if (params[i] instanceof String) {
//				params[i] = StringUtil.big5ToIso1(params[i].toString());
//			}
//		}
//		int updateCnt = jdbc.update(StringUtil.big5ToIso1(sql), params);
//		return updateCnt;
//	}
	
//	public Integer update(String sql) {
//		int updateCnt = jdbc.update(StringUtil.big5ToIso1(sql));
//		return updateCnt;
//	}
	
//	public String queryString(String sql, Object... params) {
//		String result = "";
//		for (int i = 0; i < params.length; i++) {
//			if (params[i] instanceof String) {
//				params[i] = StringUtil.big5ToIso1(params[i].toString());
//			}
//		}
//		result = (String) jdbc.queryForObject(sql, String.class, params);
//		return result;
//	}
	
	/*
	 * 回傳第一行第一列內容，通常用來回傳COUNT值
	 */
	public String queryScalar(String sql) {
		SqlRowSet rowSet = jdbc.queryForRowSet(sql);
		if (rowSet.next()) {
			return rowSet.getString(1);
		} else {
			return "";
		}
	}
	

	/**
	 * 將SQL查詢字串轉換成HTML表格
	 * 
	 * @param sSQL 資料庫查詢SQL
	 * @param sTB TABLE表格之TB內參數
	 * @param sTH TABLE表格之TH內參數
	 * @param sTR TABLE表格之TR內參數
	 * @param sTD TABLE表格之TD內參數
	 * @param bShowCount 是否顯示表格尾之加總數據
	 * @return HTML表格
	 * @throws Exception 
	 */
//	public String getHTMLTable(String sSQL, String sTB, String sTH, String sTR, String sTD, Boolean bShowCount) throws Exception {
//		Connection conn = null;
//		Statement stmt = null;
//		ResultSet rs = null;
//		ResultSetMetaData rsmd = null;
//		int numberOfColumns = 0;
//		int rowCounter = 0;
//		StringBuffer Output = new StringBuffer();
//
//		try {
//
//			log.debug("DBUtil.getHTMLTable SQL=" + sSQL);
//			conn = jdbc.getDataSource().getConnection();
//			stmt = conn.createStatement();
//			rs = stmt.executeQuery(sSQL);
//
//			// 建立表格<table>
//			if (StringUtil.isEmpty(sTB))
//				Output.append("<table border=1 cellspacing=0 cellpadding=0>\n");
//			else
//				Output.append("<table ").append(sTB).append(">\n");
//
//			// 建立表頭<th>
//			rsmd = rs.getMetaData();
//			numberOfColumns = rsmd.getColumnCount();
//			Output.append("<tr>\n");
//			Output.append("<th ").append(sTH).append(">").append("#").append("</th>\n");
//			for (int i = 1; i <= numberOfColumns; i++) {
//				Output.append("<th ").append(sTH).append(">").append(rsmd.getColumnName(i)).append("</th>\n");
//			}
//			Output.append("</tr>\n");
//
//			// 建立表列<tr>
//			while (rs.next()) {
//				rowCounter++;
//				Output.append("<tr ").append(sTR).append(">\n");
//				Output.append("<td ").append(sTD).append(">").append(rowCounter).append("</td>\n");
//				for (int i = 1; i <= numberOfColumns; i++) {
//					String field = rs.getString(i);
//					field = (StringUtil.isEmpty(field) ? "&nbsp;" : field);
//					Output.append("<td ").append(sTD).append(">").append(field).append("</td>\n");
//				}
//				Output.append("</tr>\n");
//			}
//
//			// 顯示表尾資料筆數
//			if (bShowCount) {
//				// 無資料時回傳
//				if (rowCounter == 0) {
//					Output.append("<tr ").append(sTR).append("><td ").append(sTD).append(" colspan=\"")
//							.append(numberOfColumns + 1).append("\"><strong>查無資料!!!</strong></td></tr>\n");
//				} else {
//					Output.append("<tr ").append(sTR).append("><td ").append(sTD).append(" colspan=\"")
//							.append(numberOfColumns + 1).append("\">資料筆數共計：").append(rowCounter)
//							.append(" 筆</td></tr>\n");
//				}
//			}
//			Output.append("</table>\n");
//		} catch (Exception e) {
//			log.error("DBUtil.getHTMLTable FAILED,REASON=" + e.toString());
//			throw e;
//		} finally {
//			// 清理並回收資源
//			if (rs != null) {
//				try {
//					rs.close();
//				} catch (Exception e) {
//				}
//			}
//			if (stmt != null) {
//				try {
//					stmt.close();
//				} catch (Exception e) {
//				}
//			}
//			if (conn != null) {
//				try {
//					conn.close();
//				} catch (Exception e) {
//				}
//			}
//		}
//		return Output.toString();
//	}

	/**
	 * 格式化SQL字串
	 * @param strPattern SQL樣板
	 * @param oArg 參數
	 * @return 格式化後之SQL字串
	 */
	public String formatSQL(String strPattern, Object[] oArg) {
		return MessageFormat.format(strPattern, oArg);
	}
	

	/**
	 * 呼叫預存程式取得RAISE ERROR MESSAGE
	 * @param type 傳入參數
	 * @throws Exception
	 */
//	public void raiseCall(String type) throws Exception {
//		final String procCall = "{call proc_0_kjsoft ?}";
//		try(	Connection conn = jdbc.getDataSource().getConnection();
//				CallableStatement cstmt = conn.prepareCall(procCall)	) {
//			
//			cstmt.setString(1, type);
//			ResultSet rs = cstmt.executeQuery();
//			rs.next();
//			log.info("ProcViewDao.raiseCall.rs: " + rs.getString(0));
//			
//		} catch (SQLException e) {
//			String code = e.getErrorCode() + "\t";
//			String message = StringUtil.iso1ToBig5(e.getMessage());
//			log.error(code + message);
//			throw new Exception(code + message);
//		}
//	}
	
	/**
	 * 呼叫預存程式取得RETURN值
	 * @param type 傳入參數
	 * @throws Exception
	 */
	public List<String> returnCall(String type) throws Exception {
		
		List<String> message = new LinkedList<>();
		final String procCall = "{? =call proc_0_kjsoft ?}";
		try(	Connection conn = jdbc.getDataSource().getConnection();
				CallableStatement cstmt = conn.prepareCall(procCall)) {
			
			cstmt.registerOutParameter(1, Types.INTEGER);
			cstmt.setString(2, type);
			Boolean isRS = cstmt.execute();
			if( !isRS ) {
				String output = String.valueOf(cstmt.getInt(1));
				message.add(output);
				log.info("ProcViewDao.returnCall.output: " + output);
			}
			return message;
		} catch (SQLException e) {
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * 呼叫預存程式取得PRINT
	 * @param type 傳入參數
	 * @throws Exception
	 */
//	public List<String> printCall(String type) throws Exception {
//		
//		List<String> message = new LinkedList<>();
//		final String procCall ="{? = call proc_0_kjsoft ?}";
//		try(	Connection conn = jdbc.getDataSource().getConnection();
//				CallableStatement cstmt = conn.prepareCall(procCall)) {
//			
//			cstmt.registerOutParameter(1, Types.VARCHAR);
//			cstmt.setString(2, type);
//			Boolean isRS = cstmt.execute();
//			if( ! isRS ) {
//				String output = cstmt.getString(1);
//				log.info("ProcView.printCall.output: " + output);
//				SQLWarning warning = cstmt.getWarnings();
//				if(warning != null) {
//					message.add(warning.getMessage());
//					log.warn("ProcViewDao.printCall.message: " + StringUtil.iso1ToBig5(warning.getMessage()));
//				}
//			}
//			return message;
//			
//		} catch (SQLException e) {
//			throw new Exception(e.getMessage());
//		}
//	}
	
	/**
	 * 呼叫預存程式取得PRINT
	 * @param type 傳入參數
	 * @throws Exception
	 */
//	public List<Map<String, Object>> resultCall(String type) throws Exception {
//		
//		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
//		final String procCall = "{call proc_0_kjsoft ?}";
//		try(	Connection conn = jdbc.getDataSource().getConnection();
//				CallableStatement cstmt = conn.prepareCall(procCall)) {
//			
//			cstmt.setString(1, type);
//			ResultSet rs = cstmt.executeQuery();
//			while(rs.next()) {
//				Map<String, Object> map = new HashMap<>();
//				map.put("comNo", rs.getString(1));
//				map.put("comCName", StringUtil.iso1ToBig5(rs.getString(2)));
//				map.put("comType", rs.getString(3));
//				map.put("dollarCode", rs.getString(4));
//				map.put("warGroup", rs.getString(5));
//				map.put("mantMax", rs.getDouble(6));
//				result.add(map);
//			}
//			return result;
//			
//		} catch (SQLException e) {
//			throw new Exception(e.getMessage());
//		}
//	}

	/**
	 * 配置 "欄位名稱"。
	 * @param affixType 附加的類型。
	 * @param param 本類內容的參數。
	 * @param rowSet 資料列表。
	 * @return 欄位名稱集合。
	 */
//	private List<String> getColumns(JdbcAffixType affixType, Object param, SqlRowSet rowSet) {
//		
//		List<String> columns = new LinkedList<>();
//		SqlRowSetMetaData meta = rowSet.getMetaData();
//		int count = meta.getColumnCount();
//
//		if (JdbcAffixType.WITH_ORIGINAL_COLUMNS == affixType) {
//
//			// 原始欄位名稱。
//			for (int i = 0; i < count; i++) {
//				columns.add(this.getColumnName(meta, (i + 1)));
//			}
//
//			// (2) 使用 "指定" 名稱，取代 "原始" 欄位名。
//		} else if (JdbcAffixType.WITH_NAMED_COLUMNS == affixType) {
//			
//			// 如："named[0]", "named[1]", third, forth
//			String[] namedColumns = (String[]) param;
//			int paramLength = namedColumns.length;
//			for (int i = 0; i < count; i++) {
//				if (i < paramLength) {
//					columns.add(namedColumns[i]);
//				} else {
//					String name = this.getColumnName(meta, (i + 1));
//					if (name == null) {
//						name = "F" + i;
//					}
//				}
//			}
//			
//		// (3) 使用 "指定" 名稱，取代 "空值" 欄位名。
//		} else if (JdbcAffixType.WITH_NO_NAME_COLUMNS == affixType) {
//			
//			// 如：first, second, "no_name[0]", forth, "no_name[1]" 
//			int index = 0;
//			String[] noNameColumns = (String[])param;	// 指定 "未命名" 欄位的名稱。
//			for (int i = 1; i <= count; i++) {
//				String name = this.getColumnName(meta, i);
//				if (name == null) {
//					name = noNameColumns[index++];
//				}
//				columns.add(name);
//			}
//
//		// (4) 使用 "數字" 當資料欄位名稱。
//		} else if (JdbcAffixType.WITH_NUMBER_COLUMNS == affixType) {
//		
//			// 如: "F0", "F1"...
//			String prefix = (String)param;	// 指定前置字元。
//			if (prefix == null || prefix.length() == 0) {
//				prefix = "F";
//			}
//			for (int i=0; i < count; i++) {
//				columns.add(prefix + i);
//			}				
//		}
//		
//		return columns;
//	}

	/**
	 * 配置 "欄位名稱"。
	 * @param affixType 附加的類型。
	 * @param param 本類內容的參數。
	 * @param rs 資料列表。
	 * @return 欄位名稱集合。
	 * @throws SQLException
	 */
//	private List<String> getColumns(JdbcAffixType affixType, Object param, ResultSet rs) throws SQLException {
//		
//		List<String> columns = new LinkedList<>();
//		ResultSetMetaData meta = rs.getMetaData();
//		int count = meta.getColumnCount();
//		
//		if (JdbcAffixType.WITH_ORIGINAL_COLUMNS == affixType) {
//
//			// 原始欄位名稱。
//			for (int i = 0; i < count; i++) {
//				columns.add(this.getColumnName(meta, (i + 1)));
//			}
//
//			// (2) 使用 "指定" 名稱，取代 "原始" 欄位名。
//		} else if (JdbcAffixType.WITH_NAMED_COLUMNS == affixType) {
//			
//			// 如："named[0]", "named[1]", third, forth
//			String[] namedColumns = (String[]) param;
//			int paramLength = namedColumns.length;
//			for (int i = 0; i < count; i++) {
//				if (i < paramLength) {
//					columns.add(namedColumns[i]);
//				} else {
//					String name = this.getColumnName(meta, (i + 1));
//					if (name == null) {
//						name = "F" + i;
//					}
//				}
//			}
//			
//		// (3) 使用 "指定" 名稱，取代 "空值" 欄位名。
//		} else if (JdbcAffixType.WITH_NO_NAME_COLUMNS == affixType) {
//			
//			// 如：first, second, "no_name[0]", forth, "no_name[1]" 
//			int index = 0;
//			String[] noNameColumns = (String[])param;	// 指定 "未命名" 欄位的名稱。
//			for (int i = 1; i <= count; i++) {
//				String name = this.getColumnName(meta, i);
//				if (name == null) {
//					name = noNameColumns[index++];
//				}
//				columns.add(name);
//			}
//
//		// (4) 使用 "數字" 當資料欄位名稱。
//		} else if (JdbcAffixType.WITH_NUMBER_COLUMNS == affixType) {
//		
//			// 如: "F0", "F1"...
//			String prefix = (String)param;	// 指定前置字元。
//			if (prefix == null || prefix.length() == 0) {
//				prefix = "F";
//			}
//			for (int i=0; i < count; i++) {
//				columns.add(prefix + i);
//			}				
//		}
//		
//		return columns;
//	}
	
	/**
	 * 查詢Store Procdure。
	 * @param affixType 附加的類型。
	 * @param param 本類內容的參數。
	 * @param sql SQL語法。
	 * @param args SQL語法的參數集合。
	 * @return 查詢結果(含中文字碼轉型)。
	 * @throws CustomException 
	 */
//	private List<Map<String, Object>> query(JdbcAffixType affixType, Object param, String sql, Object... args)
//			throws CustomException {
//		
//		// 1. 執行 SQL/SP 語法。
//		SqlRowSet rowSet = null;
//		try {
//			try {
//				rowSet = jdbc.queryForRowSet(sql, args);
//			} catch (UncategorizedSQLException e) {
//				SQLException se = e.getSQLException();
//				log.error("error code = " + se.getErrorCode());
//				log.error("error message = " + StringUtil.iso1ToBig5(se.getMessage()));
//				log.error("state = " + se.getSQLState());
//				throw e;
//			}
//		} catch (Exception ex) {
//			throw new CustomException("4014", this.getClass().getSimpleName(), ex);
//		}
//		
//		// 2. 依據 JdbcAffixType 設定值，配置 "欄位名稱"。
//		List<String> columns = this.getColumns(affixType, param, rowSet);
//		int count = columns.size();
//		
//		// 3. 獲取：所有資料行，並裝填成資料映射。
//		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
//		while (rowSet.next()) {
//			
//			// (1) 創建一個資料行映射。
//			Map<String, Object> record = new LinkedHashMap<String, Object>();	// 確保順序。
//			// (2) 裝填：欄位名稱與欄位值。
//			for (int i=0; i < count; i++) {
//				record.put(columns.get(i), rowSet.getObject(i + 1));
//			}
//			// (3) 加到集合中。
//			result.add(record);
//		}
//		
//		// 4. 轉換 ISO1 字碼為 BIG5 字碼, 並返回。
//		log.info("query: result = " + result.size());
//		return StringUtil.iso1ToBig5(result);
//	}

	/**
	 * 取得：欄位名稱。
	 * @param meta 欄位名訊息。
	 * @param index 序號。
	 * @return 欄位名稱
	 */
//	private String getColumnName(SqlRowSetMetaData meta, int index) {
//		
//		// 1. 取資料庫的欄位定義名稱。
//		String name = StringUtil.allowNullString(meta.getColumnName(index));
//		// 2. 取資料庫的欄位別名。
//		if (name == null) {
//			name = StringUtil.allowNullString(meta.getColumnLabel(index));
//		}
//		return name;
//	}
	
	/**
	 * 取得：欄位名稱。
	 * @param meta 欄位名訊息。
	 * @param index 序號。
	 * @return 欄位名稱
	 * @throws SQLException 
	 */
//	private String getColumnName(ResultSetMetaData meta, int index) throws SQLException {
//		
//		// 1. 取資料庫的欄位定義名稱。
//		String name = StringUtil.allowNullString(meta.getColumnName(index));
//		// 2. 取資料庫的欄位別名。
//		if (name == null) {
//			name = StringUtil.allowNullString(meta.getColumnLabel(index));
//		}
//		return name;
//	}
	
}
