package com.tdcc.futures.dao;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tdcc.futures.util.DBUtil;


/**
 * 
 * @author sarah
 * @creation 2021年2月3日上午10:46:14
 */
@Repository
public class CityDao {

	private static Logger log = LoggerFactory.getLogger(CityDao.class);
	
	@Autowired
	DBUtil dbUtil;
	
	public List<Map<String, Object>> getQueryList(){
		String sql = "select * from cities";
		List<Map<String, Object>> list = dbUtil.queryForList(sql);
		return list;
	} 
	
}
