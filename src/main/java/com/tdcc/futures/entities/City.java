package com.tdcc.futures.entities;

/**
 * 
 * @author sarah
 * @creation 2021年2月3日上午10:11:51
 */
public class City {

	private Integer id;
	private String name;
	private Integer population;

	public City() {
		super();
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the nameString
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param nameString the nameString to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the population
	 */
	public Integer getPopulation() {
		return population;
	}

	/**
	 * @param population the population to set
	 */
	public void setPopulation(Integer population) {
		this.population = population;
	}
	
}
